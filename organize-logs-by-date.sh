#!/bin/bash
for f in *.ulg
do
  data=`date "+%Y-%m-%d" -r "$f"`
  echo $data
  if ! `test -d $data`
  then 
	mkdir $data
  fi
  mv -v "$f" $data/
done
