#!/bin/bash
for f in *.ulg 
do
	echo "$f"
	IFS='_-'
	read -ra str <<< "$f"

	if [ "${str[0]}" = "log" ] 
	then
		folder_name="${str[2]}-${str[3]}-${str[4]}"
		file_name="${str[5]}_${str[6]}_${str[7]}"
		mv -v "$f" "$file_name"
	else 
		folder_name=`date "+%Y-%m-%d" -r "$f"`
		file_name="$f"
	fi	
	
	if ! `test -d "$folder_name"` 
	then 
		mkdir "$folder_name"
	fi

	mv -v "$file_name" "$folder_name"/
done




